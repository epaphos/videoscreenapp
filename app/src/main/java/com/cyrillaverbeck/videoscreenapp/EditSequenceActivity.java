package com.cyrillaverbeck.videoscreenapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class EditSequenceActivity extends AppCompatActivity {

    SequencesData sequences = SequencesData.getInstance();
    Sequence currentSequence;
    int index;
    ArrayList<String> list;
    SimpleArrayAdapter adapter;
    ListView listviewCurrentSequence;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_sequence);
        Intent intent = getIntent();
        index = intent.getIntExtra("Sequence Index", 0);
        currentSequence = sequences.getSequence(index);

        listviewCurrentSequence = (ListView) findViewById(R.id.edit_sequence_list_view);

        list = new ArrayList<>();

        for (int i=0; i<currentSequence.getCommandList().size(); i++){
            Command cmd = currentSequence.getCommandList().get(i);
            list.add(cmd.toString());
        }

        adapter = new SimpleArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);
        listviewCurrentSequence.setAdapter(adapter);

        listviewCurrentSequence.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);

                Toast.makeText(view.getContext(), item + " selected", Toast.LENGTH_LONG).show();

            }

        });
        listviewCurrentSequence.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);
                sequences.getSequence(index).deleteCommand(position);
                //TODO: Find more elegant solution to update ListView
                list.remove(position);
                rebuildAdapter();
                adapter.notifyDataSetChanged();
                return true;
            }
        });
        listviewCurrentSequence.setOnDragListener(new AdapterView.OnDragListener() {

            @Override
            public boolean onDrag(View v, DragEvent event) {
                Log.i("Drag","dragging");
                return false;
            }

        });
        //TODO: Implement rearringing by dragging which seems way too complicated

    }
    public void rebuildAdapter(){
        //TODO: Find more elegant solution to update ListView
        adapter = new SimpleArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);
        listviewCurrentSequence.setAdapter(adapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sequences.saveDataToFile();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_sequence, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        } else if (id == R.id.action_add) {

            AlertDialog.Builder popup = new AlertDialog.Builder(this);

            popup.setTitle("Add New command");
            String[] choice = Command.Type.availableCommands();
            popup.setSingleChoiceItems(choice, 0, null);

            LayoutInflater linf = LayoutInflater.from(this);
            final View inflator = linf.inflate(R.layout.alert_add_sequence,null );
            popup.setView(inflator);

            final EditText et1 = (EditText) inflator.findViewById(R.id.command);

            popup.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    String s1 = et1.getText().toString();
                    ListView lw = ((AlertDialog)dialog).getListView();
                    Object checkedItem = lw.getAdapter().getItem(lw.getCheckedItemPosition());
                    Command cmd = new Command(Command.Type.parseString((String) checkedItem), s1);
                    currentSequence.addCommand(cmd);
                    //TODO: Find more elegant solution to update ListView
                    list.add(cmd.toString());
                    rebuildAdapter();
                    adapter.notifyDataSetChanged();
//                    sequences.saveDataToFile();
                }
            });

            popup.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.cancel();
                }
            });
            popup.create();
            popup.show();
        }

        return super.onOptionsItemSelected(item);
    }
}


