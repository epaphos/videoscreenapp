package com.cyrillaverbeck.videoscreenapp;

import android.util.Log;
import android.util.Property;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by cyrill on 2015-06-04.
 */
public class Sequence implements Serializable {
    private String name;
    private ArrayList<Command> commandList;

    public Sequence(String name) {
        this.name = name;
        commandList = new ArrayList<Command>();
    }

    public ArrayList<Command> getCommandList() {
        return commandList;
    }
    public void addCommand(Command newCommand) {
        this.commandList.add(newCommand);
    }

    public void deleteCommand(int index) {
        this.commandList.remove(index);
    }
    public Command get(int index) {
        if (index < this.commandList.size()){
            return this.commandList.get(index);
        } else return null;
    }
    public int count(){
        return this.commandList.size();
    }

    public String getName() {
        return name;
    }
    public void setName(String new_name){
        name = new_name;
    }
}
