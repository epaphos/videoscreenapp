package com.cyrillaverbeck.videoscreenapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;


public class MainActivity extends AppCompatActivity {

    SequencesData sequences = SequencesData.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        sequences.setContext(getApplicationContext());
        sequences.loadDataFromFile();

        createButtons();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        } else if (id == R.id.action_sequences) {
            Intent intent = new Intent(this, EditAndViewAllSequencesActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void createButtons() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.LinearAdd);
        for (int i=0;i<sequences.getAllSequences().size();i++) {
            Button sequenceButton = new Button(this);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT );
            sequenceButton.setLayoutParams(layoutParams);
            sequenceButton.setText(sequences.getAllSequences().get(i).getName());
            linearLayout.addView(sequenceButton);
            final int finalIndex = i;
            sequenceButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    int server_port = Integer.parseInt(sharedPref.getString("Port", ""));
                    String IP = sharedPref.getString("IP-Address","");

                    if (!sequences.getAllSequences().isEmpty() && finalIndex < sequences.getAllSequences().size()) {
                        Context context = getApplicationContext();
                        CharSequence text = ("Sending Command Sequence now. \n To: " + IP + ":" + server_port );
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();

                        new UDPSequence(sequences.getSequence(finalIndex), server_port, IP).execute();
                    }
                }
            });
        }
    }
    private class UDPSequence extends AsyncTask<String, Integer, Boolean> {
        Sequence sequenceToSend;
        String targetIP = "127.0.0.1";
        int server_port = 10002;

        public UDPSequence(Sequence sequence, int port, String IP){
            this.sequenceToSend = sequence;
            this.targetIP = IP;
            this.server_port = port;
        }

        @Override
        protected Boolean doInBackground(String... params) {
            String messageStr="No Message";
            DatagramSocket socket = null;
            InetAddress local = null;

            try {
                socket = new DatagramSocket();
                local = InetAddress.getByName(targetIP);
                Log.i("VideoScreenApp", "Sending from Port:" + socket.getLocalPort() + " to " + local.toString() + ":" + server_port);
            } catch (SocketException e) {
                Log.i("VideoScreenApp", "Failed to create Datagram Socket");
                e.printStackTrace();
            } catch (UnknownHostException e) {
                Log.i("VideoScreenApp", "Failed to get InetAddress");
                e.printStackTrace();
            }
            for (int i=0; i<this.sequenceToSend.count();i++){
                messageStr=sequenceToSend.get(i).toString()+"\n";
                int msg_length=messageStr.length();
                byte[] message = messageStr.getBytes();
                DatagramPacket packet = new DatagramPacket(message, msg_length,local,server_port);
                try {
                    socket.send(packet);
                    Log.i("UDP Package:",messageStr);
                    if (this.sequenceToSend.get(i).getTypeOfCommand() == Command.Type.WAITFORMS) {
                        int sleepTime = Integer.parseInt(this.sequenceToSend.get(i).getPayloadString());
                        Thread.sleep(sleepTime);
                    }
                } catch (IOException e) {
                    Log.i("VideoScreenApp", "Failed to send Packet");
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    Log.i("VideoScreenApp", "Thread interupted while sleeping");
                    e.printStackTrace();
                }
            }
            Log.i("VideoScreenApp", "doInBackgroundFinished");
            socket.disconnect();
            socket.close();
            
            return null;
        }
    }


}
