package com.cyrillaverbeck.videoscreenapp;

import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A placeholder fragment containing a simple view.
 */
public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);

        updatePreferenceSummary();
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.i("Pref:", "preferences changed");
        if (key.equals("Port")) {
            Preference connectionPref = findPreference(key);
            connectionPref.setSummary("Port: " + sharedPreferences.getString(key, ""));

        }
        if (key.equals("IP-Address")) {
            Preference connectionPref = findPreference(key);
            connectionPref.setSummary("Target IP-Address: " + sharedPreferences.getString(key, ""));
            Log.i("Pref: ", "IP changed");
        }
    }

    private void updatePreferenceSummary() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        Preference pref = findPreference("Port");
        pref.setSummary("Target Port: " + sharedPreferences.getString("Port",""));

        pref = findPreference("IP-Address");
        pref.setSummary("Target IP-Address: " + sharedPreferences.getString("IP-Address",""));

    }
}
