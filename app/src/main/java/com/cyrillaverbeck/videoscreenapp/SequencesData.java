package com.cyrillaverbeck.videoscreenapp;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;


public class SequencesData implements Serializable {

    private static final SequencesData singletonSequencesData = new SequencesData();
    private ArrayList<Sequence> allSequences;
    private String saveFileName = "Sequences.data";
    private Context context = null;
    private SequencesData(){
        allSequences = new ArrayList<>();
        if (allSequences.isEmpty()) {
            Sequence sequenceTest = new Sequence("Sequence Test 1");
            createTestCommandSequence(sequenceTest);
            addSequence(sequenceTest);

        }
    }

    //Context needed to load and save data to file.
    public void setContext(Context context) {
        this.context = context;
    }

    public boolean saveDataToFile() {
        if (context !=null) {
            File file = new File(context.getFilesDir(), saveFileName);

            try {
                ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
                //Just saving the data to a file prevents from having to instances of the Singleton.
                oos.writeObject(this.getAllSequences());
                oos.close();
                return true;
            } catch (Exception e) {
                Log.i("File", "Save file didn't work");
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    public boolean loadDataFromFile() {
        if (context != null) {
            File file = new File(context.getFilesDir(), saveFileName);

            try {
                ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
                this.allSequences = (ArrayList<Sequence>) ois.readObject();
                ois.close();
                Log.i("File", "Retrieved Data from file.");
                return true;
            } catch (Exception e) {
                Log.i("File", "Load file didn't work");
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    public static SequencesData getInstance() {return singletonSequencesData;}

    public ArrayList<Sequence> getAllSequences() {
        return allSequences;
    }

    public void addSequence(Sequence sequence){
        allSequences.add(sequence);
    }

    public void removeSequence(int index) {
        allSequences.remove(index);
    }

    public Sequence getSequence(int index) {
        return allSequences.get(index);
    }

    private void setAllSequences(ArrayList<Sequence> newData) {
        this.allSequences = newData;
    }

    private void createTestCommandSequence(Sequence sequence) {
        sequence.addCommand(new Command(Command.Type.START, "Video1"));
        sequence.addCommand(new Command(Command.Type.WAITFORMS, "500"));
        sequence.addCommand(new Command(Command.Type.STOP, "Video1"));
        sequence.addCommand(new Command(Command.Type.START, "Video2"));
        sequence.addCommand(new Command(Command.Type.WAITFORMS, "2000"));
        sequence.addCommand(new Command(Command.Type.FADE, "start"));
        sequence.addCommand(new Command(Command.Type.WAITFORMS, "200"));
        sequence.addCommand(new Command(Command.Type.FADE, "stop"));
        sequence.addCommand(new Command(Command.Type.STOP, "Video2"));
    }

}
