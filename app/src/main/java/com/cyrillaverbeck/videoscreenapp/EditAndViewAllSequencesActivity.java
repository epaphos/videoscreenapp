package com.cyrillaverbeck.videoscreenapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class EditAndViewAllSequencesActivity extends AppCompatActivity {

    SequencesData sequences = SequencesData.getInstance();
    SimpleArrayAdapter adapter;
    ArrayList<String> list;
    ListView listviewAllSequences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_and_view_all_sequences);

        listviewAllSequences = (ListView) findViewById(R.id.listviewAllSequences);

        list = new ArrayList<>();
        //TODO: Maintaining another ArrayList is not necessary
        for (int i=0; i<sequences.getAllSequences().size(); i++){
            String sequenceName = sequences.getAllSequences().get(i).getName();
            list.add(sequenceName);
        }
        adapter = new SimpleArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);
        listviewAllSequences.setAdapter(adapter);

        listviewAllSequences.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);
                Intent intent = new Intent(view.getContext(), EditSequenceActivity.class);
                intent.putExtra("Sequence Index", position);
                startActivity(intent);
            }

        });
        listviewAllSequences.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);
                sequences.removeSequence(position);
                list.remove(position);
                rebuildAdapter();
                adapter.notifyDataSetChanged();
                return true;
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        sequences.saveDataToFile();
    }

    public void rebuildAdapter(){
        //TODO: Find more elegant solution to update ListView
        adapter = new SimpleArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);
        listviewAllSequences.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_sequences, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        } else if (id ==R.id.action_add) {

            LayoutInflater linf = LayoutInflater.from(this);
            final View inflator = linf.inflate(R.layout.alert_add_sequence, null);
            AlertDialog.Builder alert = new AlertDialog.Builder(this);

            alert.setTitle("Add Sequence");
            alert.setMessage("Name the new Sequence");
            alert.setView(inflator);

            final EditText et1 = (EditText) inflator.findViewById(R.id.command);

            alert.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    String s1=et1.getText().toString();
                    sequences.addSequence(new Sequence(s1));
                    list.add(s1);
                    rebuildAdapter();
                    adapter.notifyDataSetChanged();
                }
            });

            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.cancel();
                }
            });

            alert.show();
        }
        return super.onOptionsItemSelected(item);
    }

}
