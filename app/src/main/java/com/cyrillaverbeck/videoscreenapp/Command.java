package com.cyrillaverbeck.videoscreenapp;

import java.io.Serializable;

/**
 * Created by cyrill on 2015-06-04.
 */
public class Command implements Serializable {
    public enum Type{
        START,
        STOP,
        FADE,
        WAITFORMS;

        public String toString() {
            return name().toLowerCase();
        }
        public static String[] availableCommands() {
            Type[] types = values();
            String[] names = new String[types.length];

            for (int i = 0; i < types.length; i++) {
                names[i] = types[i].toString();
            }
            return names;
        }
       public static Type parseString(String string) {
           String typeAsString = string;
           if (typeAsString.toLowerCase().equals(Type.START.toString())) {
               return Type.START;
           } else if (typeAsString.toLowerCase().equals(Type.STOP.toString())) {
               return Type.STOP;
           } else if (typeAsString.toLowerCase().equals(Type.FADE.toString())) {
               return Type.FADE;
           } else if (typeAsString.toLowerCase().equals(Type.WAITFORMS.toString())) {
               return Type.WAITFORMS;
           }
           return null;
       }
    }


    private Command.Type typeOfCommand;
    private String payloadString;

    public Command(Command.Type typeOfCommand, String payloadString) {
        this.typeOfCommand = typeOfCommand;
        this.payloadString = payloadString;
    }

    public String getPayloadString() {
        return payloadString;
    }
    public void setPayloadString(String payloadString) {
        if (this.typeOfCommand == Type.WAITFORMS) {
            //TODO: Assert that payload is right format
            this.payloadString = payloadString;
        } else if (this.typeOfCommand == Type.FADE){
            this.payloadString = "start";
        } else {
            this.payloadString = payloadString;
        }
    }

    public Type getTypeOfCommand() {
        return typeOfCommand;
    }
    public void setTypeOfCommand(Type typeOfCommand) {
        this.typeOfCommand = typeOfCommand;
    }

    @Override
    public String toString() {
        return this.typeOfCommand.toString() + " " + this.payloadString;
    }
}
